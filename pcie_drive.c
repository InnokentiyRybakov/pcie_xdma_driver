#include <linux/init.h>
#include <linux/module.h>
#include <linux/errno.h>
#include <linux/pci.h>
#include <linux/interrupt.h>

#define DRV_NAME		"pru_pcie_drive"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Raitec");



static int xtrx_probe(struct pci_dev *pdev, const struct pci_device_id *id)
{
	int err; 

	err = pci_enable_device(pdev);
	if (err) {
		dev_err(&pdev->dev, "Cannot enable PCI device, "
			"aborting.\n");
		return err;
	}
	return 0;
}

static void xtrx_remove(struct pci_dev *pdev)
{
	pci_disable_device(pdev);
}


static struct pci_device_id xtrx_pci_table[] = {
	{ PCI_DEVICE(0x10EE, 0x7012),
	  .driver_data = 0 }
};

static struct pci_driver xtrx_driver = {
	.name		= DRV_NAME,
	.id_table	= xtrx_pci_table,
	.probe		= xtrx_probe,
	.remove		= xtrx_remove
};

static int __init xtrx_init( void ) 
{
	int err;
	struct pci_dev *my_pcie; 

	err = pci_register_driver(&xtrx_driver);
	if (err) {
		printk( "Unable to register PCI driver: %d\n", err);
		goto failed_pci;
	}
	printk("PCIe device is 0x10EE:0x7012 \n");

	my_pcie = pci_get_device(0x10EE, 0x7012, NULL);
	
	printk("PCIe vendor is 0x%x \n", my_pcie->vendor);
	printk("PCIe device is 0x%x \n", my_pcie->device);
	printk("PCIe class is 0x%x \n", my_pcie->class);
	printk("PCIe sub vendor is 0x%x \n", my_pcie->subsystem_vendor);
	printk("PCIe sub device is 0x%x \n", my_pcie->subsystem_device);

	return 0;

	failed_pci:
		return err;
}

static void __exit xtrx_cleanup( void ) 
{	
	pci_unregister_driver(&xtrx_driver);

}

module_init( xtrx_init );
module_exit( xtrx_cleanup );
